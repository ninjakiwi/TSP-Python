# Traveling salesman problem
import brute
import math
import time
import random
import bisect
import operator

def setup():
    size(800, 700)
    textAlign(CENTER)
    global draw_box_size, node_num, distances, nodes, calcNodeDist, randomIndexByVal, buttons, path, bestDistance, timing, geneticOn, endOn, antOn
    draw_box_size = (width - 200,height)
    
    calcNodeDist = lambda node1, node2: sqrt(abs(node1.pos[0]-node2.pos[0])**2+abs(node1.pos[1]-node2.pos[1])**2)
    randomIndexByVal = lambda valList: bisect.bisect_left(valList, random.random() * valList[-1])

    geneticOn = False
    antOn = False
    
    endOn = [0]
    node_num = 10
    nodes = []
    path = [] # nodes ordered by index and draw on screen
    distances = [] # [smallest node index of the two][difference in node index]
    bestDistance = 9999999999999
    timing = [0,0] # most recent time taken for a thing
    buttons = [Button("Remap", newMap, False), Button("Clear Path", clearPath, False), Button("Brute Force", startBrute, True), Button("Greedy", greedy, True), Button("Circle", circle, True), Button("Genetic", genetic, False), Button("Ant Colony", startAnt, True), Button("Switch Neighbors", startSwitch, False), Button("Full 2-opt", startFlip, False), Button("2-opt", startOldFlip, False), Button("Close", exit, False)]
    newMap()
    
def accumulate(iterable, func=operator.add):
    'Return running totals'
    # accumulate([1,2,3,4,5]) --> 1 3 6 10 15
    # accumulate([1,2,3,4,5], operator.mul) --> 1 2 6 24 120
    it = iter(iterable)
    try:
        total = next(it)
    except StopIteration:
        return
    yield total
    for element in it:
        total = func(total, element)
        yield total
    
def draw():
    if geneticOn:
        geneticStep()
    elif antOn:
        antStep()
    
    # draw stuff v
    background(255)
    noStroke()
    fill(230)
    rect(0,0,draw_box_size[0],draw_box_size[1])
    stats()
    
    strokeWeight(1)
    for button in range(len(buttons)):
        noStroke()
        buttons[button].drawit(button)
        stroke(50)
        line(draw_box_size[0], Button.Height * (button + 1) - 1, width, Button.Height * (button + 1) - 1)
    
    strokeWeight(2)
    stroke(60)
    for nodePI in range(len(path) - 1):
        line(nodes[path[nodePI]].pos[0], nodes[path[nodePI]].pos[1], nodes[path[nodePI+1]].pos[0], nodes[path[nodePI+1]].pos[1])
    
    for node in nodes:
        node.drawit()
        
def stats():
    global path
    fill(50)
    textSize(15)
    text("+", width - 190, height - 20)
    text("-", width - 190, height - 10)
    text(node_num, width - 170, height - 15)
    text(timing[1]- timing[0], width - 100, height - 15)
    text(pathScore(path), width - 170, height -50)
    
def newMap():
    global nodes, distances, bruteCache
    clearPath() # remove any path for the old configureation
    nodes = [Node(20, 20, 120)]
    nodes += [Node() for i in range(node_num - 1)]
    distances = []
    for node in range(len(nodes)-1):
        distances.append([])
        for compare_node in range(node + 1, len(nodes)):
            distances[node].append(calcNodeDist(nodes[node],nodes[compare_node]))
    for button in buttons:
        button.clearCache()
            
def clearPath():
    global path
    path = []
    
class Node:
    Size = 10
    def __init__(self, x = None, y = None, fill_ = 50):
        if x == None or y == None:
            x = random.random() * (draw_box_size[0] - 40) + 20
            y = random.random() * (draw_box_size[1] - 40) + 20
        self.pos = (x, y)
        self.shade = fill_
        
    def drawit(self):
        noStroke()
        fill(self.shade)
        ellipse(self.pos[0], self.pos[1], Node.Size, Node.Size)
        
class Button:
    Height = 50
    Width = 200
    def __init__(self, txt_, action, doCache_):
        self.pos = (draw_box_size[0], Button.Height) # not real y pos
        self.txt = txt_
        self.function = action
        self.doCache = doCache_
        self.pathCache = []
        self.distanceCache = 0
        self.timeCache = [0, 0]
        
    def clicked(self):
        global nodes, path, timing, geneticOn, antOn
        geneticOn = False
        antOn = False
        if self.doCache == True:
            if len(self.pathCache) >= len(nodes):
                path = self.pathCache[:]
                #timing = self.timeCache
            else:
                self.updateCache(self.function())
                #self.timeCache = timing
        else:
            self.function()
            
    def updateCache(self, path_):
        self.pathCache = path_[:]
        self.distanceCache = int(pathScore(path_))
            
    def clearCache(self):
        self.pathCache = []
        self.distanceCache = 0
        
    def drawit(self, num):
        if mouseX > self.pos[0] and mouseY > self.pos[1] * num and mouseY < self.pos[1] * (num + 1):
            if mousePressed == True: # clicked
                fill(200)
            else: # hovering
                fill(220)
        else: # mouse not on button
            fill(240)
        rect(self.pos[0], self.pos[1] * num, Button.Width, Button.Height - 1) # -1 for black line
        fill(50)
        textSize(20)
        text(self.txt, self.pos[0] + Button.Width/2, self.pos[1] * num + Button.Height/2 + 5)
        textSize(10)
        text(num,self.pos[0] + Button.Width - 10, self.pos[1] * num + Button.Height/2)
        if self.doCache and self.distanceCache > 0:
            fill(70)
            text("Distance: {}".format(self.distanceCache), self.pos[0] + Button.Width/2, self.pos[1] * num + Button.Height/2 + 18)
        
def mousePressed():
    if mouseX > width - 200:
        for button in range(len(buttons)):
            if mouseY > buttons[button].pos[1] * button and mouseY < buttons[button].pos[1] * (button + 1):
                buttons[button].clicked()
                break
            
def keyPressed():
    global node_num, buttons
    if key == "-":
        node_num -= 1
    elif key == "+" or key == "=":
        node_num += 1
    elif key == "q":
        node_num += 100
    elif key == "w":
        node_num -= 100
    for i in range(len(buttons)):
        if str(key) == str(i):
            print("hi")
            buttons[i].clicked()
        

############## Brute Force Method ###############

def startBrute():
    global bestDistance,nodeLen, timing, ending
    timing[0] = time.time()
    
    nodeLen = len(nodes)
    bestDistance = 99999999999999
    if endOn == [0]:
        ending = True
    else:
        ending = False
    if nodeLen <= 13:
        if endOn == [0]:
            brute([0], len(nodes)-1, 0)
        else:
            bruteOnLoop([0], 1, 0)
    else:
        print("Not attempting as it would take {} steps!".format(math.factorial(nodeLen)))
        return []
            
    timing[1] = time.time()
    return path
    
def brute(selected, step, pathDist):
    global bestDistance, nodeLen
    pathDistTo0 = pathDist + nodeDiff(selected[-1], 0)
    
    if step <= 0:
        if pathDistTo0 < bestDistance:
            global path, bruteCache
            bestDistance = pathDistTo0
            path = selected + [0]
            bruteCache = path
    elif pathDist > bestDistance or pathDistTo0 > bestDistance:
        pass
    else:
        for node in range(nodeLen):
            if node not in selected:
                brute(selected + [node], step-1, pathDist + nodeDiff(selected[-1], node))
                
def bruteNoLoop(selected, step, pathDist):
    global bestDistance, path, nodeLen, bruteCache

    if nodeLen <= step:
        if pathDist < bestDistance:
            bestDistance = pathDist
            path = selected
            bruteCache = path
    elif pathDist > bestDistance:
        pass
    else:
        for node in range(nodeLen):
            if node not in selected:
                bruteNoLoop(selected + [node], step+1, pathDist + nodeDiff(selected[-1], node))
                
def nodeDiff(node, node2):
    minimum = min(node, node2)
    return distances[minimum][max(node, node2) - minimum - 1] # -1 because lists start at 0

############## Circle Method ###############

def circle():
    global path, timing
    timing[0] = time.time()
    
    middle = [0,0]
    for node in nodes:
        middle[0] += node.pos[0]
        middle[1] += node.pos[1]
    middle[0] /= len(nodes)
    middle[1] /= len(nodes)
    
    angles = []
    for node in range(len(nodes)):
        angles.append(angleToNode(middle, nodes[node]))
    
    minAngle = angles[0]
    orderedNodes = [0]
    
    for angle in range(len(angles)):
        if angles[angle] < minAngle:
            angles[angle] += PI*2

    while len(orderedNodes) < len(nodes):
        smallest = 99
        smallestNode = -1
        for angle in range(1, len(angles)):
            if angles[angle] > minAngle and angles[angle] < smallest:
                smallestNode = angle
                smallest = angles[angle]
        minAngle = smallest
        orderedNodes.append(smallestNode)
    path = orderedNodes + [0]
    
    timing[1] = time.time()
    return path
    
def angleToNode(mid, node2):
    return atan2(node2.pos[1] - mid[1], node2.pos[0] - mid[0])

############### Genetic Method ################

def genetic():
    global geneticOn, gen, children, childNum, topScore, timing, childScores, randomness
    timing[0] = time.time()
    
    topScore = 0
    
    geneticOn = True
    gen = 0
    randomness = 0.1
    childNum = 100
    children = []
    childScores = [9999999999 for i in range(childNum)]
    newItem = [i for i in range(len(nodes))]
    newItem.remove(0)
    for i in range(childNum):
        random.shuffle(newItem)
        children.append(newItem[:])
    bestScore = 9999999
    
def geneticStep():
    global children, childScores, timing, topScore, path, gen
    gen += 1
    text("Generation: {}".format(gen), width - 100, height - 100)
    totalScore = 0
    for child in range(len(children)): # add score to end of array
        score = pathScore([0] + children[child] + [0])
        totalScore += score
        childScores[child] = score
        if score > topScore:
            topScore = score
            path = [0] + children[child] + [0]

    children = orderArray(children, childScores)
            
    newChildren = []
    
    newChildren.append(children[0])
    for child in range(len(children)-1):
        newChildren.append(combineLists(children[child], children[child + 1]))
        if random.random() < randomness:
            random.shuffle(newChildren[-1])
    children = newChildren
        
    timing[1] = time.time()
    
def pathScore(nodeList):
    pathLength = 0
    for node in range(len(nodeList) - 1):
        pathLength += nodeDiff(nodeList[node], nodeList[node + 1])
    return pathLength

def combineLists(ls1, ls2):
    if len(ls1) != len(ls2):
        return
    segmentLength = int(random.random() * (len(ls1)-1) + 1)
    segmentStart = int(random.random() * (len(ls1) - segmentLength - 1))
    newList = []
    for part in range(segmentStart):
        for i in ls2:
            if i in ls1[segmentStart:segmentStart + segmentLength] or i in newList:
                continue
            newList.append(i)
            break;
    newList += ls1[segmentStart:segmentStart + segmentLength]
    for part in range(segmentStart + segmentLength, len(ls1)):
        for i in ls2:
            if i in ls1[segmentStart:segmentStart + segmentLength] or i in newList:
                continue
            newList.append(i)
            break;
    return newList
        

def orderArray(theArray, sortBy):
    if len(theArray) != len(sortBy):
        return
    
    newArray = []
    
    smallest = -1
    smallestScore = 9999999
    for obj in theArray:
        smallest = -1
        smallestScore = 9999999
        for i in range(len(sortBy)):
            if sortBy[i] < smallestScore and not sortBy[i] < 0:
                smallest = i
                smallestScore = sortBy[i]
        newArray.append(theArray[smallest])
        sortBy[smallest] = -1
    return newArray
        
        
################## Greedy #####################

def greedy():
    global path, timing
    timing[0] = time.time()
    
    notSelected = [i for i in range(1,len(nodes))]
    path = [0]
    while notSelected != []:
        smallestDist = 99999999999
        nextNode = -1
        for nodeNum in notSelected:
            nodeDist = nodeDiff(path[-1], nodeNum)
            if nodeDist < smallestDist:
                smallestDist = nodeDist
                nextNode = nodeNum
        notSelected.remove(nextNode)
        path.append(nextNode)
    path = path + [0]
    
    timing[1] = time.time()
    return path

################# Ant colony ##################

def startAnt():
    global walkedCount, timing, antOn, antNum, iterations, nodes, bestScore
    timing[0] = time.time()
    
    bestScore = 100000000
    antNum = 200
    iterations = 4
    
    walkedCount = []
    for node in range(len(nodes)-1):
        walkedCount.append([])
        for compare_node in range(node + 1, len(nodes)):
            walkedCount[node].append(0)
    
    antOn = True
    # walkedCount = [0 for node in range(len(nodes))]
    return []
    
def antStep():
    global walkedCount, nodes, iterations, bestScore, antOn, path
    
    walkedCount = []
    for node in range(len(nodes)-1):
        walkedCount.append([])
        for compare_node in range(node + 1, len(nodes)):
            walkedCount[node].append(0)
    
    for ant in range(antNum):
        currentPath = [0]
        options = range(1,len(nodes))
        for thing in range(len(options)):
            next = getNextNode(options, walkedCount, currentPath[-1])
            currentPath.append(next)
            options.remove(next)
        currentPath += [0]
        
        pathLen = pathScore(currentPath)
        
        for node in range(1,len(currentPath)):
            walkedWeight(currentPath[node-1],currentPath[node], 1/pathLen)
        
        if pathLen < bestScore:
            path = currentPath
            bestScore = pathLen
        
        
    if iterations <= 0:
        global buttons, timing
        buttons[6].updateCache(path)
        timing[1] = time.time()
        buttons[6].timeCache = timing
        antOn = False
        return 0
    else:
        iterations -= 1
    
def getNextNode(ops, walkCnt, currentNode):
    probArray = []
    total = 0
    for option in ops:
        probArray.append((walkedWeight(currentNode, option) + 1)/(nodeDiff(currentNode, option)**10))
        total += probArray[-1]
    num = random.random() * total
    upto = 0
    for prob in range(len(probArray)):
        upto += probArray[prob]
        if num < upto:
            return ops[prob]
    print("Failed")
    return None
        
def walkedWeight(node, node2, add_val = 0):
    global walkedCount
    minimum = min(node, node2)
    return walkedCount[minimum][max(node, node2) - minimum - 1] + add_val # -1 because lists start at 0
        

############################################Switcher###############################################

def startSwitch():
    global path
    bestLen = pathScore(path)
    madeChange = True

    while madeChange == True:
        madeChange = False
        for node in range(len(path)):
            comp = node + 1
            if node + 1 >= len(path):
                comp = 0
            path[node], path[comp] = path[comp], path[node]
            newScore = pathScore(path)
            if newScore < bestLen:
                bestLen = newScore
                madeChange = True
            else:
                path[node], path[comp] = path[comp], path[node]
        #############NEED TO ADD TEST FOR FIRST AND LAST###############

######################################## Flipper #########################################

def startFlip():
    global path
    bestLen = pathScore(path)
    madeChange = True
    while madeChange == True:
        madeChange = False
        for point1 in range(len(path)-2):
            for point2 in range(point1+1, len(path)-1):
                path[point1+1:point2+1] = path[point1+1:point2+1][::-1]
                newScore = pathScore(path)
                if newScore < bestLen:
                    bestLen = newScore
                    #madeChange = True
                else:
                    path[point1+1:point2+1] = path[point1+1:point2+1][::-1]
                # if linesCross(nodes[path[point1]].pos, nodes[path[point1 + 1]].pos, nodes[path[point2]].pos, nodes[path[point2 + 1]].pos) == True:
                #     path[point1+1:point2+1] = path[point1+1:point2+1][::-1]
                #     madeChange = True

#################################### Old flip ###################################

def startOldFlip():
    global path
    bestLen = pathScore(path)
    madeChange = True
    while madeChange == True:
        madeChange = False
        for point1 in range(len(path)-2):
            for point2 in range(point1+1, len(path)-1):
                if linesCross(nodes[path[point1]].pos, nodes[path[point1 + 1]].pos, nodes[path[point2]].pos, nodes[path[point2 + 1]].pos) == True:
                    path[point1+1:point2+1] = path[point1+1:point2+1][::-1]
                    madeChange = True
                    
def linesCross(p1,p2,p3,p4):
    if ((p2[0]-p1[0])*(p4[1]-p2[1])-(p2[1]-p1[1])*(p4[0]-p2[0])) * ((p2[0]-p1[0])*(p3[1]-p2[1])-(p2[1]-p1[1])*(p3[0]-p2[0])) < 0:
        if ((p4[0]-p3[0])*(p2[1]-p4[1])-(p4[1]-p3[1])*(p2[0]-p4[0])) * ((p4[0]-p3[0])*(p1[1]-p4[1])-(p4[1]-p3[1])*(p1[0]-p4[0])) < 0:
            return True
    return False